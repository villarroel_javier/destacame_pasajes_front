# Frontend Pasajes Destacame

## Prerequisitos
- Git https://es.atlassian.com/git/tutorials/install-git
- Nodejs y npm https://www.npmjs.com/get-npm

## Clonar repositorio
```
git clone https://villarroel_javier@bitbucket.org/villarroel_javier/destacame_pasajes_front.git
```

## Instalar dependencias del proyecto
```
cd destacame_pasajes_front/
npm install
```

## Correr aplicación
```
npm run serve
```

## Consideraciones importantes
- La aplicación Frontend supone que el Backend se encuentra disponible en la dirección http://127.0.0.1:8000/
- En caso de que no fuera así, es necesario modificar el archivo "main.js", ubicado en la carpeta "src"
- El apartado a modificar es el siguiente:

```
axios.defaults.baseURL = "http://127.0.0.1:8000/"; //url del backend
```

## Iniciar sesión
Para iniciar sesión es necesario tener corriendo previamente la aplicación [Backend](https://bitbucket.org/villarroel_javier/destacame_pasajes_backend/src/master/) y haber creado un usuario de acuerdo a lo especificado en el archivo Readme correspondiente

### Credenciales sugeridas
- usuario: destacame
- password: destacame123