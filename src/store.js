import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		token: localStorage.getItem('token') || null,
	},
	getters: {
		loggedIn(state){
			return state.token !== null
		}
	},
	mutations: {
		getToken(state, token){
			state.token = token
		},
		destroyToken(state){
			state.token = null
		}
	},
	actions: {
		getToken(context, credentials){
			return new Promise((resolve, reject) => {
				axios.post('/api-token-auth/login/', credentials).then(response => {
					const token = response.data.token
					localStorage.setItem('token', token)
					context.commit('getToken', token)
					axios.defaults.headers.common['Authorization'] = 'Token '+token
					resolve(response)
				})
				.catch(error => {
					reject(error)
				});
			})
		},
		destroyToken(context){
			localStorage.removeItem('token')
			context.commit('destroyToken')
		}
	}
})
