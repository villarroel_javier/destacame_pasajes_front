import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import App from './App.vue'
import axios from 'axios'

Vue.use(Router)

let router = new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'login',
			component: Login,
		},
		{
			path: '/home',
			name: 'home',
			component: () => import('./views/Home.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/journeys',
			name: 'journey',
			component: () => import('./views/Journeys.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/passengers',
			name: 'passenger',
			component: () => import('./views/Passengers.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/drivers',
			name: 'driver',
			component: () => import('./views/Drivers.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/cities',
			name: 'city',
			component: () => import('./views/Cities.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/bus',
			name: 'bus',
			component: () => import('./views/Bus.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/bus_driver',
			name: 'busdriver',
			component: () => import('./views/BusDriver.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/schedules',
			name: 'schedule',
			component: () => import('./views/Schedules.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/tickets',
			name: 'ticket',
			component: () => import('./views/Tickets.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
		{
			path: '/reports',
			name: 'reports',
			component: () => import('./views/Reports.vue'),
			meta: { 
                requiresAuth: true,
            }
		},
	]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('token') == null) {
            next({
                path: '/login',
            })
        } else {
        	axios.defaults.headers.common['Authorization'] = 'Token '+localStorage.getItem('token')
            next()
        }
    }else {
        next() 
    }
})

export default router
