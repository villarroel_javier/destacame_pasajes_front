import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store'
import router from './router'
import axios from 'axios'
import moment from 'moment'

axios.defaults.baseURL = "http://127.0.0.1:8000/";
Vue.prototype.$http = axios
Vue.prototype.moment = moment

Vue.config.productionTip = false

const eventBus = new Vue();
Vue.prototype.eventBus = eventBus;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
